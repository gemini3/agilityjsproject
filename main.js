/**
 * TourMaster - a utility for tour management.
 * Copyright (C) 2016 Miha Kitič <miha.kitic@gmail.com>

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * Function to implement 1st agility object. */
function first_agility_obj() {
    return $$({

        // Data model
        model: {

        },

        // HTML view
        view: {
            // HTML content
            format: '<div>'

                    // TODO :: !! WRITE YOUR HTML HERE !!

                + '</div>',

            // Local CSS style
            style: '& {'
                 + '    height : 100%;'
                 + '    width  : 100%;'
                 + '    border : 1px solid black;'
                 + '} '

                    // TODO :: !! ADD YOUR LOCAL CSS HERE !!
        },

        // Controller functions
        controller: {

            // Will be called on creation
            'create': function() {
                alert('Welcom to AgilityJS demo!');
            }
        },

        // TODO :: !! CREATE YOUR VARIABLES & FUNCTIONS HERE !!

    });
}
